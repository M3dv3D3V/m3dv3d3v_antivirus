import datetime
import subprocess

from pathlib import Path

from signature_analysis.file_scanner import FileScanner
from signature_analysis.memory_scanner import MemoryScanner
from updater import script
import config
from agent_db import AgentDatabase


HOME = str(Path.home())


def test_positive_infected_file():
    expected_result = [
        '/home/kali/malware_db/wanna_cry/Ransomware.WannaCry.zip',
        '/home/kali/malware_db/wanna_cry/ed01ebfbc9eb5bbea545af4d01bf5f1071661840480439c6e5babe8e080e41aa.exe'
    ]
    file_scanner = FileScanner('/home/kali/malware_db/wanna_cry')
    actual_result = [path for path in file_scanner.start_scan().keys()]
    assert expected_result == actual_result


def test_negative_infected_file():
    expected_result = {}
    file_scanner = FileScanner('/home/kali/malware_db/wdas')
    actual_result = file_scanner.start_scan()
    assert expected_result == actual_result


def test_positive_infected_process():
    expected_result = {'danger.exe': '3301'}
    memory_scanner = MemoryScanner([
        'danger.exe',
        '/home/kali/malware_db/wanna_cry/ed01ebfbc9eb5bbea545af4d01bf5f1071661840480439c6e5babe8e080e41aa.exe'
    ])
    memory_scanner.malware_processes = {'danger.exe': '3301'}
    actual_result = memory_scanner.start_scan()
    assert expected_result == actual_result


def test_negative_infected_process():
    expected_result = {}
    memory_scanner = MemoryScanner([
        '/home/kali/malware_db/wanna_cry/Ransomware.WannaCry.zip',
        '/home/kali/malware_db/wanna_cry/ed01ebfbc9eb5bbea545af4d01bf5f1071661840480439c6e5babe8e080e41aa.exe'
    ])
    actual_result = memory_scanner.start_scan()
    assert expected_result == actual_result


def test_positive_module_updater():
    script.Updater()
    result = subprocess.run(['ls', f'{HOME}/builds/actual_build.zip'])
    assert result.returncode == 0


def test_negative_module_updater():
    script.Updater()
    result = subprocess.run(['ls', f'{HOME}/builds/not_adasctual_build.zip'])
    assert result.returncode != 0


def test_positive_module_agent_db():
    connection_db_files = AgentDatabase(config.PATH_TO_DATABASE_INFECTED_PROCESSES)
    current_date = datetime.datetime.now()
    date_detected = current_date.strftime('%Y-%m-%d %H:%M:%S')
    expected_result = (f'danger_process - {current_date}', date_detected)
    connection_db_files.insert_data_to_table_infected_processes([expected_result])
    actual_result = connection_db_files.get_data_from_table('infected_processes')[-1]
    assert expected_result == actual_result[1:]