import sqlite3
import logging
import logging.config

import config


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('agent_db.py')


class AgentDatabase:
    def __init__(self, path_to_db) -> None:
        self._path_to_db = path_to_db
        self._connection_db = None
        self._cursor_db = None
        self._perform_connection_to_database()

    def _perform_connection_to_database(self):
        try:
            success_message = f'The connection to the database on the {self._path_to_db} path was successfully established'
            self._connection_db = sqlite3.connect(self._path_to_db)
            self._cursor_db = self._connection_db.cursor()
            LOGGER.info(success_message)
        except Exception:
            error_message = f'Connection to the database on the path {self._path_to_db} was not established'
            LOGGER.error(error_message)

    def _create_table_infected_files(self):
        self._cursor_db.execute("""CREATE TABLE IF NOT EXISTS infected_files(
                        id integer primary key AUTOINCREMENT,
                        path_to_file TEXT,
                        date_detect TEXT,
                        possible_name TEXT);
                        """)
        self._connection_db.commit()

    def insert_data_to_table_infected_files(self, data: list):
        self._cursor_db.executemany(
            f'INSERT INTO infected_files(path_to_file, date_detect, possible_name) VALUES(?, ?, ?);',
            data
        )
        self._connection_db.commit()

    def get_data_from_table(self, name_table: str):
        result = self._cursor_db.execute(f'SELECT * FROM {name_table}').fetchall()
        return result

    def _create_table_infected_processes(self):
        self._cursor_db.execute("""CREATE TABLE IF NOT EXISTS infected_processes(
                        id integer primary key AUTOINCREMENT,
                        process_name TEXT,
                        date_detect TEXT);
                        """)
        self._connection_db.commit()

    def insert_data_to_table_infected_processes(self, data: list):
        self._cursor_db.executemany(
            f'INSERT INTO infected_processes(process_name, date_detect) VALUES(?, ?);',
            data
        )
        self._connection_db.commit()

    def delete_data_from_table(self, name_table: str, _id: str) -> None:
        self._cursor_db.execute(f'DELETE FROM {name_table} WHERE id = {_id}')


# agent_database = AgentDatabase(config.PATH_TO_DATABASE_INFECTED_PROCESSES)
# agent_database._create_table_infected_files()
# agent_database._create_table_infected_processes()
# import datetime
# current_date = datetime.datetime.now()
# date_detected = current_date.strftime('%Y-%m-%d %H:%M:%S')
# agent_database.insert_data_to_table_infected_processes([('wannacryMalware', date_detected)])
# a = agent_database.get_data_from_table('infected_processes')
# print(a)
