import datetime
import logging
import logging.config

import agent_db
import config
import messages


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('report_builder.py')


class ReportBuilder:
    def __init__(self) -> None:
        self._agent_db_infected_files = agent_db.AgentDatabase(
            config.PATH_TO_DATABASE_INFECTED_FILES)
        self._agent_db_infected_process = agent_db.AgentDatabase(
            config.PATH_TO_DATABASE_INFECTED_PROCESSES
        )

    def build_report(self):
        result = """
            <!doctype html>
            <html lang="ru">
            <head>
            <meta charset="utf-8" />
            <title>Malware report</title>
            <link rel="stylesheet" href="style.css" />
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

                    </head>
            <body>
            <h2>Malware files.</h2>
            <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Number</th>
                    <th scope="col">Path</th>
                    <th scope="col">Date detect</th>
                    <th scope="col">Possible name</th>
                </tr>
            </thead>

            <tbody>
        """
        data_from_database = self._agent_db_infected_files.get_data_from_table('infected_files')
        for data in data_from_database:
            result += '<tr>'
            result += f'<th>{data[0]}</th>'
            result += f'<th>{data[1]}</th>'
            result += f'<th>{data[2]}</th>'
            result += f'<th>{data[3]}</th></tr>'

        result += """
            <table class="table">
            <h2>Malware processes.</h2>
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Number process</th>
                    <th scope="col">Process name</th>
                    <th scope="col">Date detect</th>
                </tr>
            </thead>

            <tbody>
        """
        data_from_database_processes = self._agent_db_infected_process.get_data_from_table(
            'infected_processes')
        for data in data_from_database_processes:
            result += '<tr>'
            result += f'<th>{data[0]}</th>'
            result += f'<th>{data[1]}</th>'
            result += f'<th>{data[2]}</th>'

        result += '</tbody></table></body></html>'

        current_date = datetime.datetime.now()
        with open(f'reports/report_{current_date}.html', 'w+') as report_file:
            report_file.write(result)

        LOGGER.info(messages.LOG_REPORT_WAS_BUILT)

        return result