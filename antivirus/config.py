COMPILED_YARA_RULES = '/home/kali/m3dv3d3v_antivirus/antivirus/signature_analysis/signature_databases/db_clamav_main_with_wanna_cry_compiled'
IS_STOP_SCAN = False

PATH_TO_DATABASE_INFECTED_FILES = f'/home/kali/m3dv3d3v_antivirus/antivirus/database_infected_files.db'
PATH_TO_DATABASE_INFECTED_PROCESSES = f'/home/kali/m3dv3d3v_antivirus/antivirus/database_infected_processes.db'

IS_ACTIVE_DELETE = False
IS_ACTIVE_QUARANTINE = False
IS_ACTIVE_SKIP = False
IS_ACTIVE_KILL = False
IS_ACTIVE_SUSPEND = False


DICT_LOG_CONFIG = {
    'version':1,
    'handlers': {
        'fileHandler': {
            'class': 'logging.FileHandler',
            'formatter': 'custom_formatter',
            'filename': 'log/global.log'
        }
    },
    'loggers': {
        'main.py': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        },
        'agent_db.py': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        },
        'script.py (updater)': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        },
        'report_builder.py': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        },
        'memory_scanner.py': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        },
        'file_scanner.py': {
            'handlers': ['fileHandler'],
            'level': 'INFO',
        }
    },
    'formatters': {
        'custom_formatter': {
            'format': '%(levelname)s - %(asctime)s - %(name)s - %(message)s'
        }
    }
}