import sys
import subprocess
import logging
import logging.config

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
from gui import Ui_MainWindow
from gui_actions import Ui_Form

import config
import messages
from report_builder import ReportBuilder
from signature_analysis.file_scanner import FileScanner
from signature_analysis.memory_scanner import MemoryScanner
from agent_db import AgentDatabase
from updater.script import BUILD_VERSION, Updater


with open('updater/build_version.txt', 'r') as build_version_file:
    BUILD_VERSION = build_version_file.read()


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('main.py')


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.pushButton_start.clicked.connect(self.start_scan)
        self.ui.pushButton_stop.clicked.connect(self.stop_scan)
        self.ui.pushButton_report.clicked.connect(self.build_report)
        self.ui.pushButton_update.clicked.connect(self.update_antivirus)
        self.ui.pushButton_exit.clicked.connect(self.btn_exit)
        self.ui.pushButton_actions.clicked.connect(self.btn_actions)

        self.ui.label.setText(f'build version: {BUILD_VERSION}')

    def start_scan(self):
        LOGGER.info(messages.LOG_SCANNING_START)

        data_from_txt_input = self.ui.lineEdit.text()
        paths = data_from_txt_input.splitlines()
        for path in paths:
            if config.IS_STOP_SCAN:
                break

            file_scanner = FileScanner(path)
            infected_file_names_and_signatures = file_scanner.start_scan(self, app)
            file_scanner.perform_action_for_gui(
                infected_file_names_and_signatures,
                self,
                app
            )

            infected_file_names = infected_file_names_and_signatures.keys()
            memory_scanner = MemoryScanner(infected_file_names)
            memory_scanner.start_scan(self, app)

        LOGGER.info(messages.LOG_SCAN_COMPLETED)

        config.IS_STOP_SCAN = False

    def stop_scan(self):
        LOGGER.info(messages.LOG_SCANNING_STOPPED)
        config.IS_STOP_SCAN = True

    def build_report(self):
        report_builder = ReportBuilder()
        report_builder.build_report()
        msg = QMessageBox()
        msg.setWindowTitle('Builder')
        msg.setText(f'{messages.BUILD_COMPLETED_SUCCESSFULLY} Report in reports directory.')
        msg.setIcon(QMessageBox.Information)
        msg.exec_()

    def update_antivirus(self):
        updater = Updater()
        result_update = updater.update()
        msg = QMessageBox()
        msg.setWindowTitle('Updater')
        msg.setText(result_update)
        msg.setIcon(QMessageBox.Information)
        msg.exec_()

    def btn_exit(self):
        LOGGER.info(messages.LOG_COMPLETE_ANTIVIRUS)
        self.close()

    def refresh_text_box(self, data): 
        self.ui.textEdit_output.append(data)
        app.processEvents() #update gui for pyqt

    def btn_actions(self):
        print('actions!')
        self.w2 = ActionsWindows()
        self.w2.show()


class ActionsWindows(QtWidgets.QWidget):
    def __init__(self):
        super(ActionsWindows, self).__init__()
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._connection_db_files = AgentDatabase(config.PATH_TO_DATABASE_INFECTED_FILES)
        self._connection_db_processes = AgentDatabase(config.PATH_TO_DATABASE_INFECTED_PROCESSES)

        self._malware_files_for_actions = list()
        self._malware_processes_for_actions = list()

        self.ui.lineEdit_from.setText('1')
        self.ui.lineEdit_to.setText('1')
        self.ui.lineEdit_range.setText('1,2')
        self._quantity_malware_files = self._get_quantity_malware_files()
        self.ui.lineEdit_quantity_malware_files.setText(self._quantity_malware_files)
        self._quantity_malware_processes = self._get_quantity_malware_processes()
        self.ui.lineEdit_quantity_processes.setText(self._quantity_malware_processes)

        self.ui.pushButton_files.clicked.connect(self.get_malware_files)
        self.ui.pushButton_processes.clicked.connect(self.get_malware_processes)
        self.ui.pushButton_delete.clicked.connect(self.delete_files)
        self.ui.pushButton_quarantine.clicked.connect(self.move_to_quarantine_files)
        self.ui.pushButton_kill.clicked.connect(self.kill_processes)
        self.ui.pushButton_suspend.clicked.connect(self.suspend_processes)

    def _get_quantity_malware_files(self) -> str:
        result = str(len(self._connection_db_files.get_data_from_table('infected_files')))
        return result

    def _get_quantity_malware_processes(self) -> str:
        result = str(len(self._connection_db_processes.get_data_from_table('infected_processes')))
        return result

    def get_malware_files(self) -> str:
        result = ''
        result_data = list()
        text_edit_from = self.ui.lineEdit_from.text()
        text_edit_to = self.ui.lineEdit_to.text()
        text_edit_range = self.ui.lineEdit_range.text().split(',')

        is_default_edit_range = text_edit_range == ['1', '2']
        if is_default_edit_range and text_edit_to != '1':
            result_data = list()
            malware_files = self._connection_db_files.get_data_from_table('infected_files')
            text_edit_from = int(text_edit_from)
            text_edit_to = int(text_edit_to)

            for data in malware_files:
                id_file = int(data[0])
                if text_edit_from <= id_file <= text_edit_to:
                    result_data.append(data)
                    result += f'{data[0]} {data[1]} {data[2]} {data[3]}\n\n'

            self.ui.textEdit_records_from_db.setText(result)

            self._malware_files_for_actions = result_data

            return result

        if text_edit_range != ['1', '2']:
            result_data = list()
            malware_files = self._connection_db_files.get_data_from_table('infected_files')
            text_edit_range = [int(digit) for digit in text_edit_range]
            
            for data in malware_files:
                id_file = int(data[0])
                if id_file in text_edit_range:
                    result_data.append(data)
                    result += f'{data[0]} {data[1]} {data[2]} {data[3]}\n\n'

                self.ui.textEdit_records_from_db.setText(result)

        self._malware_files_for_actions = result_data

        return result

    def get_malware_processes(self) -> str:
        result = ''
        result_data = list()
        text_edit_from = self.ui.lineEdit_from.text()
        text_edit_to = self.ui.lineEdit_to.text()
        text_edit_range = self.ui.lineEdit_range.text().split(',')

        is_default_edit_range = text_edit_range == ['1', '2']
        if is_default_edit_range and text_edit_to != '1':
            result_data = list()
            malware_processes = self._connection_db_processes.get_data_from_table('infected_processes')
            text_edit_from = int(text_edit_from)
            text_edit_to = int(text_edit_to)

            for data in malware_processes:
                id_file = int(data[0])
                if text_edit_from <= id_file <= text_edit_to:
                    result_data.append(data)
                    result += f'{data[0]} {data[1]} {data[2]}\n\n'

            self.ui.textEdit_records_from_db.setText(result)

            self._malware_processes_for_actions = result_data

            return result

        if text_edit_range != ['1', '2']:
            result_data = list()
            malware_processes = self._connection_db_processes.get_data_from_table('infected_processes')
            text_edit_range = [int(digit) for digit in text_edit_range]
            
            for data in malware_processes:
                id_file = int(data[0])
                if id_file in text_edit_range:
                    result_data.append(data)
                    result += f'{data[0]} {data[1]} {data[2]}\n\n'

                self.ui.textEdit_records_from_db.setText(result)

        self._malware_processes_for_actions = result_data

        return result

    def delete_files(self) -> str:
        result = ''
        for data in self._malware_files_for_actions:
            id_file = data[0]
            file_path = data[1]
            try:
                subprocess.run(['rm', file_path])
                result += f'File {file_path} successfully deleted.\n\n'
                self._connection_db_files.delete_data_from_table('infected_files', id_file)
            except:
                error_message = f'[E] File {file_path} was not deleted.\n\n'
                result += error_message
                LOGGER.error(error_message)
        
            self.ui.textEdit_records_from_db.setText(result)

        actual_quantity_malware_files = self._get_quantity_malware_files()
        self.ui.lineEdit_quantity_malware_files.setText(actual_quantity_malware_files)

        return result

    def move_to_quarantine_files(self) -> str:
        result = ''
        for data in self._malware_files_for_actions:
            id_file = data[0]
            file_path = data[1]
            try:
                subprocess.run(['mv', file_path, '/tmp'])
                result += f'File {file_path} successfully moved to quarantine zone (/tmp).\n\n'
                self._connection_db_files.delete_data_from_table('infected_files', id_file)
            except:
                error_message = f'[E] File {file_path} was not moved to quarantine zone.\n\n'
                result += error_message
                LOGGER.error(error_message)

            self.ui.textEdit_records_from_db.setText(result)

        actual_quantity_malware_files = self._get_quantity_malware_files()
        self.ui.lineEdit_quantity_malware_files.setText(actual_quantity_malware_files)

        return result

    def kill_processes(self) -> str:
        result = ''
        for data in self._malware_processes_for_actions:
            id_process = data[0]
            process_name = data[1]
            subprocess.run(['pkill', process_name])
            result += f'Process {process_name} was killed.'
            self._connection_db_processes.delete_data_from_table('infected_processes', id_process)
            self.ui.textEdit_records_from_db.setText(result)

        actual_quantity_malware_processes = self._get_quantity_malware_processes()
        self.ui.lineEdit_quantity_processes.setText(actual_quantity_malware_processes)

        return result

    def suspend_processes(self) -> str:
        result = ''
        for data in self._malware_processes_for_actions:
            process_name = data[1]
            result_cmd = subprocess.run(['pkill', '-STOP', process_name])
            if result_cmd.returncode == 0:
                result += f'Process {process_name} was suspended.'
                self.ui.textEdit_records_from_db.setText(result)
                continue

            error_message = f'Process {process_name} was not suspended.'
            result += error_message
            LOGGER.error(error_message)
            self.ui.textEdit_records_from_db.setText(result)
            

        actual_quantity_malware_processes = self._get_quantity_malware_processes()
        self.ui.lineEdit_quantity_processes.setText(actual_quantity_malware_processes)

        return result

if __name__ == '__main__':
    LOGGER.info(messages.LOG_START_ANTIVIRUS)
    app = QtWidgets.QApplication([])
    application = MainWindow()
    application.show()
    sys.exit(app.exec())
