import datetime
import subprocess
import logging
import logging.config

import yara
from PyQt5.QtWidgets import QMessageBox

import agent_db
import config
import messages


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('file_scanner.py')


class FileScanner:
    def __init__(self, path) -> None:
        self.path = path
        self._compiled_rules = config.COMPILED_YARA_RULES
        self._rules = yara.load(self._compiled_rules)
        self._files_into_path = subprocess.run(
            ['find', self.path, '-type', 'f,l'], stdout=subprocess.PIPE, text=True
        )

    def start_scan(self, self_main_gui_object=None, app=None) -> dict:
        LOGGER.info(messages.LOG_FILE_SCANNER_STARTED)
        if not self.path:
            LOGGER.error(messages.LOG_FILE_SCANNER_PATH_NOT_ENTERED)
            return '[ERROR] path not entered.'

        agent_database = agent_db.AgentDatabase(config.PATH_TO_DATABASE_INFECTED_FILES)

        result = dict()

        # here must be logging
        files = self._files_into_path.stdout
        for file_path in files.splitlines():
            if config.IS_STOP_SCAN:
                break

            current_date = datetime.datetime.now()
            date_detected = current_date.strftime('%Y-%m-%d %H:%M:%S')

            matched_signatures = self._rules.match(file_path)
            if not matched_signatures and self_main_gui_object and app:
                message_is_ok = f'[INFO] File {file_path} is Ok.\n'
                self_main_gui_object.refresh_text_box(message_is_ok)
                app.processEvents()
                continue

            infected_names_as_list = [str(item) for item in matched_signatures]
            possible_malware_names = ' '.join(infected_names_as_list)
            result[file_path] = matched_signatures

            LOGGER.info(f'File {file_path} is infected.')

            agent_database.insert_data_to_table_infected_files(
                [(file_path, date_detected, possible_malware_names)]
            )

        config.IS_STOP_SCAN = False

        LOGGER.info(messages.LOG_FILE_SCANNER_COMPLETED)

        return result

    def perform_action(self, data) -> None:
        for file_path, _ in data.items():
            print(f'[INFO] File {file_path} is infected.')
            action = input(f'Select an action with a file - {file_path}. (d-delete, i-isolation, s-skip): ')
            if action == 'd':
                subprocess.run(['rm', file_path])
                print(f'[INFO] File {file_path} was deleted.')

            if action == 'i':
                subprocess.run(['mv', file_path, '/tmp'])
                print(f'[INFO] File {file_path} moved to quarantine zone.')

            if action == 's':
                print(f'[INFO] File {file_path} was skipped.')

    def perform_action_for_gui(self, data, self_main_gui_object, app) -> None:
        for file_path, _ in data.items():
            self_main_gui_object.refresh_text_box(f'[INFO] File {file_path} is infected.')