import datetime
import subprocess
import logging
import logging.config

import agent_db
import config
import messages


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('memory_scanner.py')


class MemoryScanner:
    def __init__(self, infected_file_names) -> None:
        self.infected_file_names = infected_file_names
        self.malware_processes = dict()

    def _search_malware_process(self, infected_file_name, all_running_processes) -> list:
        result = list()
        for process_name in all_running_processes:
            if infected_file_name in process_name:
                result.append(process_name)

        return result

    def _adding_malware_process_into_general_dict(self, processes_info, infected_processes):
        agent_database = agent_db.AgentDatabase(config.PATH_TO_DATABASE_INFECTED_PROCESSES)

        for infected_process in infected_processes:
            current_date = datetime.datetime.now()
            date_detected = current_date.strftime('%Y-%m-%d %H:%M:%S')

            self.malware_processes[infected_process] = processes_info.get(infected_process)
            agent_database.insert_data_to_table_infected_processes(
                [(infected_process, date_detected)]
            )

    def start_scan(self, self_main_gui_object=None, app=None) -> dict:
        LOGGER.info(messages.LOG_MEMORY_SCANNER_STARTED)
        all_system_processes = subprocess.run(
            ['ps', 'aux'], stdout=subprocess.PIPE, text=True
        )

        # get all system process information
        processes_info = dict()
        for row in all_system_processes.stdout.splitlines():
            _, pid, *_, command = row.split()
            processes_info[command] = pid

        # searching malware processes
        for full_infected_file_name in self.infected_file_names:
            if not full_infected_file_name:
                continue

            infected_file_name = full_infected_file_name.split('/')[-1]
            infected_processes = self._search_malware_process(infected_file_name, processes_info)
            if infected_processes:
                for infected_process in infected_processes:
                    message = f'[INFO] Process {infected_process} is infected!\n'

                    LOGGER.info(message)

                    if self_main_gui_object and app:
                        self_main_gui_object.refresh_text_box(message)
                        app.processEvents()

                    self.malware_processes[infected_process] = processes_info.get(infected_process)
                self._adding_malware_process_into_general_dict(processes_info, infected_processes)

        LOGGER.info(messages.LOG_MEMORY_SCAN_COMPLETED)

        return self.malware_processes