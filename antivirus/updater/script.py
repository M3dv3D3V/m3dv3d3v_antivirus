import os
import paramiko
from pathlib import Path
import logging
import logging.config

import config
import messages


HOME = str(Path.home())

with open(f'{HOME}/m3dv3d3v_antivirus/antivirus/updater/server_address.txt', 'r') as server_address_file:
    SERVER_ADDRESS = server_address_file.read()

with open(f'{HOME}/m3dv3d3v_antivirus/antivirus/updater/username.txt', 'r') as username_file:
    USERNAME = username_file.read()

with open(f'{HOME}/m3dv3d3v_antivirus/antivirus/updater/password.txt', 'r') as password_file:
    PASSWORD = password_file.read()

with open(f'{HOME}/m3dv3d3v_antivirus/antivirus/updater/build_version.txt', 'r') as build_version_file:
    BUILD_VERSION = build_version_file.read()


logging.config.dictConfig(config.DICT_LOG_CONFIG)
LOGGER = logging.getLogger('script.py (updater)')


class Updater:
    def __init__(self) -> None:
        self._password = '1234'

        self._ssh = paramiko.SSHClient()
        self._ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))

    def update(self) -> str:
        try:
            self._ssh.connect(
                SERVER_ADDRESS,
                username=USERNAME,
                password=PASSWORD
            )
        except paramiko.ssh_exception.NoValidConnectionsError:
            error_message = f'Unable to connect to port 22 on {SERVER_ADDRESS}'
            LOGGER.error(error_message)
            print(error_message)
            self._ssh.close()
            return error_message
        except paramiko.ssh_exception.SSHException:
            error_message = f'Server "{SERVER_ADDRESS}" not found in known_hosts'
            LOGGER.error(error_message)
            print(error_message)
            self._ssh.close()
            return error_message

        LOGGER.info(messages.LOG_SSH_SESSION_OPEN)

        sftp = self._ssh.open_sftp()
        LOGGER.info(messages.LOG_SFTP_SESSION_OPEN)
        try:
            sftp.get('/builds/actual_build.zip', f'{HOME}/builds/actual_build.zip')
            return f'{messages.UPDATE_COMPLITE} New file location: {HOME}/builds/actual_build.zip'
        except FileNotFoundError:
            Path(f'{HOME}/builds/').mkdir(parents=True, exist_ok=True)
            return messages.DIRECTORY_CREATED
        finally:
            sftp.close()
            LOGGER.info(messages.LOG_SSH_SESSION_CLOSED)
            self._ssh.close()
            LOGGER.info(messages.LOG_SFTP_SESSION_CLOSED)
